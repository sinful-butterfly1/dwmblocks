//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" ", "uptime -p | sed 's/[^ ]* *//'", 1, 10},
	{" " , "pulsemixer --get-volume | awk '{print $1}'", 1, 10},
	{" ", "sensors | awk '/Core 0/ {print $3}'", 1, 10},
	{" " , "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g", 1, 10},
	{" ", "sed \"s/$/%/\" /sys/class/power_supply/BAT?/capacity", 5, 10},
	{" ", "date ",	60,	0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
